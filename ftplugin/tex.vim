imap <buffer> [[ \begin{
imap <buffer> ]] <Plug>LatexCloseCurEnv
nmap <buffer> <F5> <Plug>LatexChangeEnv
nmap <buffer> <S-F5> <Plug>LatexToggleStarEnv
vmap <buffer> <F7> <Plug>LatexWrapSelection
vmap <buffer> <S-F7> <Plug>LatexEnvWrapSelection
imap <buffer> (( \eqref{
map <buffer> <F6> :LatexTOCToggle<CR>
map  <silent> <buffer> ¶ :call LatexBox_JumpToNextBraces(0)<CR>
map  <silent> <buffer> § :call LatexBox_JumpToNextBraces(1)<CR>
imap <silent> <buffer> ¶ <C-R>=LatexBox_JumpToNextBraces(0)<CR>
imap <silent> <buffer> § <C-R>=LatexBox_JumpToNextBraces(1)<CR>
let g:LatexBox_split_width=50
let g:LatexBox_split_resize=1
let g:LatexBox_autojump=1
let g:LatexBox_latexmk_options="-pdf -f -pv -silent "
let g:LatexBox_viewer="open -a Skim.app"
let b:maplocalleader=";"

"map <buffer> <D-b> :Latexmk
" Use <Leader>v to view pdf
map <buffer> <Leader>v :!open '%:p:r.pdf'<CR>

" Use <Leader>lk to jump to line in pdf
map <buffer> <silent> <Leader>ls :silent !/Applications/Skim.app/Contents/SharedSupport/displayline
		\ <C-R>=line('.')<CR> "<C-R>=LatexBox_GetOutputFile()<CR>" "%:p" <CR>

" Set 'makeprg' so that you can use Cmd+B to compile
setl makeprg=latexmk\ -pdf\ -f\ -pv\ -silent

" Add a small space using <Option-Space>
imap <buffer> <M-Space> \,
imap <buffer> <M-6> ^{}<ESC>i
imap <buffer> <M--> _{}<ESC>i
