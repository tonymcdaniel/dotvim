APP         := my_project

#### Directories ####
SRC_DIR     := src
BLD_DIR     := build
INC_DIR     := include
INST_DIR    := /usr/local/bin

#### Source Files ####
CPPSRCS     := $(wildcard $(SRC_DIR)/*.cpp)
CPPOBJS     := $(patsubst $(SRC_DIR)/%.cpp,$(BLD_DIR)/%.o,$(CPPSRCS)) 
CSRCS       := $(wildcard $(SRC_DIR)/*.c)
COBJS       := $(patsubst $(SRC_DIR)/%.c,$(BLD_DIR)/%.o,$(CSRCS)) 
FSRCS       := $(wildcard $(SRC_DIR)/*.f)
FOBJS       := $(patsubst $(SRC_DIR)/%.f,$(BLD_DIR)/%.o,$(FSRCS)) 
HDRS        := $(wildcard $(INC_DIR)/*.h)
DISTFILES   := $(CPPSRCS) $(CSRCS) $(FSRCS) $(HDRS) Makefile Doxyfile license.txt

#### Compilers ####
CXX         := g++
FXX			:= gfortran
LXX			:= g++

#### Utilities ####
GREP		:= grep -iRn
COPY		:= cp
DELETE		:= rm -f
ARCH		:= tar -cvzf 
ARCH_EXT	:= tgz
DOCUTIL		:= doxygen
DOCSRC		:= Doxyfile

#### Options ####
CXX_OPTS    := -Wall -mtune=native -std=gnu9x
FXX_OPTS	:= -Wall -mtune=native -ffree-form -std=gnu
DBG_OPTS	:= -DDEBUG -g 
REL_OPTS    := -O3 -ftree-vectorize
PROF_OPTS   := -pg

#### Libraries and Include files ####
LIBS       := -lm					# Math Library
LIBS       += -lstdc++				# Standard C++ Library
#LIBS       += -lmetis				# Metis library for graph partitioning
#LIBS       += -lmpi -lmpi_cxx		# MPI libraries
#LIBS       += -fopenmp				# OpenMP libraries
#LIBS       += -lgsl -lgslcblas		# GNU Scientific Library
INCLUDES   := -I./include -I$(HOME)/include -I/usr/local/include
LXXFLAGS   := -L./lib -L$(HOME)/lib -L/usr/local/lib

vpath %.cpp $(SRC_DIR)
vpath %.c   $(SRC_DIR)
vpath %.f   $(SRC_DIR)
vpath %.h   $(INC_DIR)
vpath %.o   $(BLD_DIR)

.PHONY : help clean todo dist docs install

help:
	@echo "Targets in this make file --------------------------------------" 
	@echo "   make [help]  : print this info" 
	@echo "" 
	@echo "Build options --------------------------------------------------" 
	@echo "   make all     : build $(APP) with standard flags"
	@echo "   make debug   : build $(APP) with debug flags" 
	@echo "   make profile : build $(APP) with profiling flags" 
	@echo "   make release : build $(APP) with optimization flags" 
	@echo "" 
	@echo "Utilities ------------------------------------------------------" 
	@echo "   make clean   : remove all files that can be rebuilt"
	@echo "   make todo    : print a list of TODO items from source files"
	@echo "   make dist    : create a $(ARCH_EXT) archive of source files"
	@echo "   make docs    : build the documentation using $(DOCUTIL)"
	@echo "   make install : install the binary to $(INST_DIR)"

all: $(APP)

debug: CXX_OPTS += $(DBG_OPTS)
debug: FXX_OPTS += $(DBG_OPTS)
debug: $(APP)

profile: CXX_OPTS += $(REL_OPTS) $(PROF_OPTS) 
profile: FXX_OPTS += $(REL_OPTS) $(PROF_OPTS) 
profile: LXX_OPTS += $(REL_OPTS) $(PROF_OPTS)
profile: $(APP)

release: CXX_OPTS += $(REL_OPTS)
release: FXX_OPTS += $(REL_OPTS)
release: $(APP)

$(APP): $(COBJS) $(CPPOBJS) $(FOBJS) $(HDRS)
	$(LXX) -o $(APP) $(LXX_OPTS) $(LXXFLAGS) $(COBJS) $(CPPOBJS) $(FOBJS) $(LIBS)

clean:
	$(DELETE) $(COBJS) $(CPPOBJS) $(FOBJS) $(APP)

todo: 
	$(GREP) todo $(CPPSRCS) $(CSRCS) $(FSRCS) $(HDRS) 

dist:
	$(ARCH) $(APP).$(ARCH_EXT) $(DISTFILES)

docs:
	$(DOCUTIL) $(DOCSRC)

install:
	$(COPY) $(APP) $(INST_DIR)

$(BLD_DIR)/%.o: %.cpp
	$(CXX) -c $< $(CXX_OPTS) $(INCLUDES) -o $@

$(BLD_DIR)/%.o: %.c
	$(CXX) -c $< $(CXX_OPTS) $(INCLUDES) -o $@

$(BLD_DIR)/%.o: %.F
	$(FXX) -c $< $(FXX_OPTS) $(INCLUDES) -o $@


