"-----------------------------------------------------------------------------
" Global Stuff
"-----------------------------------------------------------------------------

" Get pathogen up and running
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
filetype on
syntax on
filetype plugin indent on
set wildmode=longest:full,list:full

" pretty colors
colorscheme solarized
set background=dark
if (&term =~ "xterm" || &term =~ "rxvt")
    " change cursor shape in iTerm2
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
    " Enable mouse support
    set mouse=a
    " set 256 colors
    set t_Co=256
endif

set clipboard=unnamed   " Use the system clipboard for copy/paste
set nocompatible        " Use Vim mode, not vi compatible
set nohidden            " unload a buffer when not displayed
set directory=/tmp//    " put swap files in a single location
set nobackup            " don't create backup files
set gcr=n:blinkon0      " Stop the cursor from blinking in normal mode
set go-=T               " hide the toolbar
set history=1000        " remember more commands and search history
set undolevels=1000     " use many levels of undo
set tabpagemax=1000	    " Increase the limit on number of tabs
set title               " change the terminal's title
set visualbell          " don't beep
set noerrorbells        " don't beep
let g:netrw_liststyle=1 " Set default view for file list
set nrformats=          " Always treat numbers as decimal
set autochdir

" Text display options
set wrap linebreak nolist
set columns=120
set cursorline
set showmatch

" Use the same symbols as TextMate for tab stops and EOL
set listchars=tab:▸\ ,trail:·,eol:¬
highlight NonText guifg=#4a4a59
highlight SpecialKey guifg=#4a4a59

" Status line
set laststatus=2
set statusline=
set statusline+=%-3.3n\                         " buffer number
set statusline+=%f\                             " filename
set statusline+=%h%m%r%w                        " status flags
set statusline+=\[%{strlen(&ft)?&ft:'none'}]    " file type
set statusline+=%{fugitive#statusline()}        " git status
set statusline+=%=                              " right align remainder
set statusline+=%-14(%l,%c%V%)                  " line, character
set statusline+=%<%P                            " file position
set ruler number showcmd
set numberwidth=4

" Search options
set nohlsearch " don't highlight search matches
set wrapscan   " set the search scan to wrap lines
set ignorecase " ignore case when searching
set smartcase  " unless not all lowercase 
set incsearch  " start searching immediately
set showmatch  " show match as you type
set gdefault   " apply substitution globally

" Spelling Options
let g:spelllang="en_us"
let g:tex_comment_nospell=1 " Don't spell check comments
set spellsuggest=9

"-----------------------------------------------------------------------------
" Filetype Settings
"-----------------------------------------------------------------------------

" Tabstops are 4 spaces
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set autoindent
" set smartindent

" Enable syntax highlighting.
syntax on

" Default to unix and utf-8
set fileformat=unix
set encoding=utf-8


"-----------------------------------------------------------------------------
" Tab Stuff
"-----------------------------------------------------------------------------

" Use Ctrl-Shift-[] to switch tabs
map <silent> <D-S-[> gT
map <silent> <D-S-]> gt
" Map Ctrl-# to choose tabs 1 through 9
" For normal mode
map <D-1> :tabn 1<CR>
map <D-2> :tabn 2<CR>
map <D-3> :tabn 3<CR>
map <D-4> :tabn 4<CR>
map <D-5> :tabn 5<CR>
map <D-6> :tabn 6<CR>
map <D-7> :tabn 7<CR>
map <D-8> :tabn 8<CR>
map <D-9> :tabn 9<CR>
map <D-0> :tablast<CR>
" For insert mode
map! <D-1> <C-O>:tabn 1<CR>
map! <D-2> <C-O>:tabn 2<CR>
map! <D-3> <C-O>:tabn 3<CR>
map! <D-4> <C-O>:tabn 4<CR>
map! <D-5> <C-O>:tabn 5<CR>
map! <D-6> <C-O>:tabn 6<CR>
map! <D-7> <C-O>:tabn 7<CR>
map! <D-8> <C-O>:tabn 8<CR>
map! <D-9> <C-O>:tabn 9<CR>
map! <D-0> <C-O>:tablast<CR>


"-----------------------------------------------------------------------------
" Keyboard Mappings
"-----------------------------------------------------------------------------

" ease of use keyboard mappings 
nnoremap H ^
nnoremap L $

vnoremap <Down> j
vnoremap <Up> k
vnoremap <Left> h
vnoremap <Right> l

nnoremap <silent> j gj
nnoremap <silent> k gk
vnoremap <silent> j gj
vnoremap <silent> k gk

" use TextMate Keyboard short cuts for indent/unindent
nnoremap <D-[> <<
nnoremap <D-]> >>
vnoremap <D-[> <gv
vnoremap <D-]> >gv
inoremap <D-[> <Esc><<i
inoremap <D-]> <Esc>>>i

" Bubble single lines
nmap <D-Up> [e
nmap <D-Down> ]e
omap <D-Up> [e
omap <D-Down> ]e
" Bubble multiple lines
vmap <D-Up> [egv
vmap <D-Down> ]egv

" Visually select the text that was last edited/pasted
nmap gV `[v`]

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


"-----------------------------------------------------------------------------
" Leader Key Mappings
"-----------------------------------------------------------------------------

" Leader Key shortcuts
let mapleader = ";"
let g:mapleader = ";"

" Toggle hidden characters
nmap <leader>ls :set list!<CR>
" Toggle search highlighting
nmap <leader>hl :set hlsearch!<CR>
" Toggle spelling
nmap <silent> <leader>s :set spell!<CR>
" Edit the vimrc file
nmap <leader>v :tabedit $MYVIMRC<CR>

"-----------------------------------------------------------------------------
" Template Settings
"-----------------------------------------------------------------------------

" If buffer modified, update any 'Last modified: ' in the first 20 lines.
" 'Last modified: ' can have up to 10 characters before (they are retained).
" Restores cursor and window position using save_cursor variable.
function! LastModified()
    if &modified
        let save_cursor = getpos(".")
        let n = min([20, line("$")])
        keepjumps exe '1,' . n . 's#^\(.\{,10}Last Modified : \).*#\1' .
                    \ strftime('%a %b %d, %Y  %I:%M%p') . '#e'
        call histdel('search', -1)
        call setpos('.', save_cursor)
    endif
endfun

" When creating a new file, insert the file name and date created.
function! NameDate()
    let save_cursor = getpos(".")
    let n = min([20, line("$")])
    keepjumps exe '1,' . n . 's#^\(.\{,10}Creation Date : \).*#\1' .
                \ strftime('%a %b %d, %Y  %I:%M%p') . '#e'
    keepjumps exe '1,' . n . "g/File Name :.*/s//File Name : " .expand("%")
    keepjumps exe '1,' . n . "g/Copyright.*/s//Copyright (c) " .
                \ strftime('%Y') . ' Tony R. McDaniel'
    call histdel('search', -3)
    call setpos('.', save_cursor)
endfun

" When creating a new file, check for a template and load it
function! LoadTemplate()
    silent! 0r ~/.vim/templates/tmpl.%:e
    " Highlight %VAR% placeholders with the Todo colour group
    syn match Todo "%\u\+%" containedIn=ALL
    call NameDate()
endfunction

if has("autocmd")
    autocmd! BufNewFile *.cpp call LoadTemplate()
    autocmd! BufNewFile *.c call LoadTemplate()
    autocmd! BufNewFile *.py call LoadTemplate()
    autocmd! BufNewFile *.tex call LoadTemplate()
    autocmd! BufNewFile *.sh call LoadTemplate()
    autocmd! BufNewFile Makefile call LoadTemplate()
endif

"Jump between %VAR% placeholders in Normal mode with <Ctrl-p>
nnoremap <C-p> /%\u.\{-1,}%<cr>c/%/e<cr>

"Jump between %VAR% placeholders in Insert mode with <Ctrl-p>
inoremap <C-p> <ESC>/%\u.\{-1,}%<cr>c/%/e<cr>

"-----------------------------------------------------------------------------
" Auto Commands
"-----------------------------------------------------------------------------
if has("autocmd")
    autocmd bufwritepost .vimrc source $MYVIMRC
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
    autocmd BufNewFile * set fileformat=unix encoding=utf-8
    " autocmd! BufNewFile * call LoadTemplate()
    autocmd BufWritePost .vimrc source %
    autocmd! BufWritePre * call LastModified()
endif

"-----------------------------------------------------------------------------
" Useful functions
"-----------------------------------------------------------------------------
function! SetExecutableBit()
  let fname = expand("%:p")
  checktime
  execute "au FileChangedShell " . fname . " :echo"
  silent !chmod a+x %
  checktime
  execute "au! FileChangedShell " . fname
endfunction
command! Xbit call SetExecutableBit()

"-----------------------------------------------------------------------------
" Function Keys
"-----------------------------------------------------------------------------
" F1 File Browser
nnoremap <silent> <F1> :NERDTreeToggle<cr>
inoremap <silent> <F1> <ESC>:NERDTreeToggle<cr>

" F2 Jump to buffer
nnoremap <F2> :buffers<CR>:buffer<Space>

" F12 ShowFuncKeys
function! ShowFuncKeys(bang)
    for i in range(1,12)
        redir! => map
        exe "silent ".(a:bang == "!" ? 'verbose' : '') . " map<F".i.">"
        redir end
        if map !~ 'No mapping found'
            echomsg map
        endif
    endfor
endfunction

com! -bang ShowFuncKeys :call ShowFuncKeys(<q-bang>)
nnoremap <silent> <F12> :ShowFuncKeys<CR>
inoremap <silent> <F12> <ESC>:ShowFuncKeys<CR>


"-----------------------------------------------------------------------------
" Plugin Settings
"-----------------------------------------------------------------------------
" For UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
"let g:UltiSnipsSnippetDirectories=["UltiSnips","snippets"]
let g:UltiSnipsSnippetDirectories=["UltiSnips","MySnips"]

" For TagList
nnoremap <silent> <F13> :TlistToggle<CR>
let Tlist_Ctags_Cmd = "/usr/local/bin/ctags"
let Tlist_WinWidth = 50
map <leader>ta :TlistToggle<cr>
map <leader>bta :!/usr/local/bin/ctags -R .<CR>
set tags=tags;/

" For YankRing
nnoremap <silent> <F3> :YRShow<CR>

" For Gundo
nnoremap <silent> <F4> :GundoToggle<CR>

" For Dash.app
let g:dash_map = {
        \ 'python'     : 'python2',
        \ }
nmap <silent> <leader>d <Plug>DashSearch

" For Bclose
nnoremap <D-d> :Bclose<CR>

" For Lusty Juggler
let g:LustyJugglerDefaultMappings = 0
map <leader>b :LustyJuggler<cr>
let g:LustyJugglerShowKeys = 'a'

" Use LaTeX instead of plain tex
let g:tex_flavor="latex"
" No folding in LaTeX files
let g:tex_fold_enabled=0

" Python-mode
" Don't automatically use rope
let g:pymode_rope=0
" Don't fold functions by default
let g:pymode_folding=0
" Don't check on every save
let g:pymode_lint_write=0

" Use modern format for fortran files
let fortran_free_source=1


" Use // comments for C type files
if has("autocmd")
    autocmd FileType c,cpp,cs,java setlocal commentstring=//\ %s
endif
" vim: filetype=vim
